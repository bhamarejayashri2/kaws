# KAWS 
kaws is a Kit for AWS, a CLI utility for performing regular aws operations meant for enhancing developer experience.

Install globally by executing `npm i kaws --global`

Supported services as of now:
- KMS (encrypt/decrypt a secret using an existing symmetric key)
- STS (auth check)
- CloudWatch Logs (tail CloudWatch Logs on the terminal)

```sh
$ npm install -g .
$ kaws encrypt --help
╭──────────╮
│          │
│   kaws   │
│          │
╰──────────╯
kaws encrypt [alias] [secret] [region]

encrypt your secret using KMS key

Positionals:
  alias   Required. the kms key alias                                             [string]
  secret  Required. the secret to be encrypted                                    [string]
  region  Optional. aws region hosting kms key             [string] [default: "us-east-2"]

Options:
  --version  Show version number                                       [boolean]
  --help     Show help                                                 [boolean]
```

```sh
$ kaws encrypt --alias alias/sample --secret mySuperPassword
╭──────────╮
│          │
│   kaws   │
│          │
╰──────────╯
AQICAHiK9JPD5+FhCgtDG0LLEHvzQ4bSp4zYHJcyrTuQWK4SQgFkdhKBtViNpt6Q0KNaq4pZAAAAbTBrBgkqhkiG9w0BBwagXjBcAgEAMFcGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMfM3JZKDLK0m/4r1CAgEQgCrtYcPwqgTVtCXwC5bmRaOmsbTNR1kvdfKT3VP5TqiAlFcad/+mpHK/AtA=
```

```sh
$ kaws decrypt --cipher AQICAHiK9JPD5+FhCgtDG0LLEHvzQ4bSp4zYHJcyrTuQWK4SQgFkdhKBtViNpt6Q0KNaq4pZAAAAbTBrBgkqhkiG9w0BBwagXjBcAgEAMFcGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMfM3JZKDLK0m/4r1CAgEQgCrtYcPwqgTVtCXwC5bmRaOmsbTNR1kvdfKT3VP5TqiAlFcad/+mpHK/AtA=
╭──────────╮
│          │
│   kaws   │
│          │
╰──────────╯
mySuperPassword
```

```sh
$ kaws whoami
╭──────────╮
│          │
│   kaws   │
│          │
╰──────────╯
{
  UserId: 'AIDAUDSOMEUSERID3FRVL5FSB',
  Account: '281234567893',
  Arn: 'arn:aws:iam::281234567893:user/someuser'
}
```

```sh
$ kaws cwlogs --logGroupName /aws/ecs/myEcsCluster/myApp/ --region eu-central-1 --filterPattern ERROR
╭──────────╮
│          │
│   kaws   │
│          │
╰──────────╯
[ERROR] A fatal exception has occurred
[ERROR] A fatal exception has occurred again.
[ERROR] A fatal exception has occurred yet again.
^C
$
```

