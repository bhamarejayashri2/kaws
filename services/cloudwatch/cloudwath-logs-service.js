'use strict';

const TAIL_DELAY=5000

function sleep(milliSec){
  return new Promise((resolve,reject)=>{
    setTimeout(resolve, milliSec)
  })
}

class CloudWatchLogsService {
  constructor(cwClient){
    this.cwClient = cwClient;
  }

  async getLogEvents(logGroupName, logStreamNamePrefix, filterPattern){
    if (!logGroupName) throw new Error('Must provide log group name')
    try{
      while(true){
        const params = {
          logGroupName: logGroupName,
          logStreamNamePrefix: logStreamNamePrefix,
          filterPattern: filterPattern,
          limit: 20,
          interleaved: true,
          startTime: (new Date()) - TAIL_DELAY,
          endTime: new Date().getTime()
        }
        const result = await this.cwClient.filterLogEvents(params).promise();
        if (!result){
          console.log('No logs found.')
        }
        result.events.forEach(event => {
          console.log(event.message);
        });
        await sleep(TAIL_DELAY);
      }
    }catch(e){
      console.log(e.message);
    }
  }
}

module.exports = {
  CloudWatchLogsService,
}