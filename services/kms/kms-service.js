'use strict';

class KmsService{
  constructor(kmsClient){
    this.kmsClient = kmsClient;
  }

  async encrypt(alias,secret){
    if (!alias) throw new Error('Must provide kms key alias');
    if (!secret) throw new Error('Must provide secret to be encrypted')
    const params = {
      KeyId : alias,
      Plaintext : secret
    };
    try{
        const { CiphertextBlob } = await this.kmsClient.encrypt(params).promise();
        return CiphertextBlob.toString('base64');
       }
    catch(ex){
        console.log(ex.message);
        throw ex;
    } 
  }

  async decrypt(CiphertextBlob){
    if (!CiphertextBlob) throw new Error('Must provide encrypted cipher.')
    const params = {
      CiphertextBlob : Buffer.from(CiphertextBlob,'base64')
    };
    try{
      const { Plaintext } = await this.kmsClient.decrypt(params).promise();
      return Plaintext.toString();
    }
    catch(ex){
      console.log(ex.message);
    }
  }
}


module.exports = { KmsService }