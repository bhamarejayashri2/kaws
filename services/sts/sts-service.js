'use strict';

class StsService{
  constructor(stsClient){
    this.stsClient = stsClient;
  }

  async validateCredentials(){
    try{
      const  {UserId, Account, Arn}  =  await this.stsClient.getCallerIdentity().promise();
      return {UserId, Account, Arn}
    }
    catch(ex){
      console.log(ex.message);
    }
  }
}

module.exports = { StsService }