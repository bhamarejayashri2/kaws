'use strict';

const { KmsService } = require('../../../../services/kms/kms-service');

describe('kms service happy scenario', () => {
  it('should encrypt the password', async () => {
    const mockPromise = jest.fn().mockResolvedValue({CiphertextBlob : Buffer.from('mysecretkey', 'base64')});
    const mockEncrypt = jest.fn().mockImplementation(()=>{
      return {promise: mockPromise}
    })
    const kmsMockClient = {
      encrypt: mockEncrypt
    }
  
    const kms = new KmsService(kmsMockClient);
    const encpwd = await kms.encrypt('jayashrib','mysecretkey');
    console.log(encpwd);

    expect(encpwd).toBe('mysecretkew=');
    expect(mockEncrypt).toHaveBeenCalledWith({KeyId: 'jayashrib', Plaintext: 'mysecretkey'})
  })
})

describe('kms service unhappy scenario', () => {
  it('should throw an error', async () => {
    
    const mocPromise = jest.fn().mockRejectedValue(new Error('somthing bad!!!'))
    const mocEncrypt = jest.fn().mockImplementation(() => {
      return { promise : mocPromise}
    })
    const kmsMocClient = {
      encrypt : mocEncrypt
    }

    const kms = new KmsService(kmsMocClient);
    try{
      await kms.encrypt('jayashri','mypassword');
    }
    catch(e){
      expect(e.message).toBe('somthing bad!!!')
    }
  })
})