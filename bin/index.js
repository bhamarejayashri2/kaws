#!/usr/bin/env node

const chalk = require('chalk');
const boxen = require('boxen');
const yargs = require('yargs');

const { 
  encryptArgs, 
  decryptArgs, 
  authArgs,
  cwLogsArgs,
 } = require('./args');
const { 
  encryptCommand, 
  decryptCommand, 
  authCommand,
  cwLogsCommands,
 } = require('./commands');

const greeting = chalk.white.bold('kaws');

const boxenOptions = {
  padding : 1,
  margine : 1,
  borderStyle : 'round',
  borderColor : 'red',
  backgroundColor : '#554444'
};

const msgbox = boxen(greeting,boxenOptions);

console.log(msgbox);



yargs
  .scriptName("kaws")
  .usage('$0 <cmd> [args]')
  .version('1.0.1')
  .command('encrypt [alias] [secret] [region]', 'encrypt your secret using KMS key', encryptArgs, encryptCommand)
  .command('decrypt [cipher]', 'decrypt your cipherBlob using KMS key', decryptArgs, decryptCommand)
  .command('whoami [region]', 'Validates your AWS credentials', authArgs, authCommand)
  .command('cwlogs [logGroupName] [streamPrefix] [filterPattern]', 'tails cloudwatch logs on terminal', cwLogsArgs, cwLogsCommands)
  .help()
  .argv
