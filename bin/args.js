'use strict';

function encryptArgs(yargs){
  yargs.positional('alias', {
    type: 'string',
    describe: 'Required. the kms key alias'
  }),
  yargs.positional('secret', {
    type: 'string',
    describe: 'Required. the secret to be encrypted'
  }),
  yargs.positional('region', {
    type: 'string',
    default : 'us-east-2',
    describe: 'Optional. aws region hosting kms key '
  })
}

function decryptArgs(yargs){
  yargs.positional('cipher', {
    type: 'string',
    describe: 'Required. the secret cipherBlob'
  }),
  yargs.positional('region', {
    type: 'string',
    default : 'us-east-2',
    describe: 'Optional. aws region hosting kms key '
  })
}

function authArgs(yargs){
  yargs.positional('region', {
    type: 'string',
    default : 'us-east-2',
    describe: 'Optional. aws region hosting kms key '
  })
}

function cwLogsArgs(yargs){
  yargs.positional('logGroupName', {
    type: 'string',
    describe: 'Required. AWS CloudWatch Log group name.'
  }),
  yargs.positional('streamPrefix', {
    type: 'string',
    describe: 'Optional. AWS CloudWatch Log Stream prefix.'
  }),
  yargs.positional('filterPattern', {
    type: 'string',
    describe: 'Optional. The pattern in the logs to look for'
  })
  yargs.positional('region', {
    type: 'string',
    default: 'us-east-2',
    describe: 'Optional. aws region hosting the log group'
  })
}

module.exports = {
  encryptArgs,
  decryptArgs,
  authArgs,
  cwLogsArgs,
}