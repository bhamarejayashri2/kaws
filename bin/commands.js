'use strict';

const {KMS, STS, CloudWatchLogs} = require('aws-sdk');
const { KmsService } = require('../services/kms/kms-service');
const { StsService } = require('../services/sts/sts-service');
const { CloudWatchLogsService } = require('../services/cloudwatch/cloudwath-logs-service');

async function decryptCommand(argv){
  const kms = new KMS({region : argv.region});
  const decryptor = new KmsService(kms);
  try {
    console.log(await decryptor.decrypt(argv.cipher));
  }catch(e){
    console.log(e.message);
  }
}

async function encryptCommand(argv){
  const kms = new KMS({region : argv.region});
  const encryptor = new KmsService(kms);
  try {
    console.log(await encryptor.encrypt(argv.alias,argv.secret));
  } catch (e) {
    console.log(e.message)
  }
}

async function authCommand(argv){
  const sts = new STS({ region : argv.region});
  const auth = new StsService(sts);
  try {
    console.log(await auth.validateCredentials());    
  } catch (e) {
    console.log(e.message);
  }

}

async function cwLogsCommands(argv){
  const cwClient = new CloudWatchLogs({ region : argv.region});
  const cwLogs = new CloudWatchLogsService(cwClient);
  try{
    await cwLogs.getLogEvents(argv.logGroupName, argv.streamPrefix, argv.filterPattern)
  }catch(e){
    console.log(e.message);
  }
}

module.exports = {
  decryptCommand,
  encryptCommand,
  authCommand,
  cwLogsCommands,
}